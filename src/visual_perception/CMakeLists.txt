set(visual_perception_files
        VisualPerception.cc
        VisualPerception.h
        VisualPerceptionMemory.cc
        VisualPerceptionMemory.h
        VisualEmulator.cc
        VisualEmulator.h
        )
add_library(visual_perception ${visual_perception_files})

