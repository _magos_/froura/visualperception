/*
----------------------------------------------
--- Author         : Ahmet Özlü
--- Mail           : ahmetozlu93@gmail.com
--- Date           : 1st August 2017
--- Version        : 1.0
--- OpenCV Version : 2.4.10
--- Demo Video     : https://youtu.be/3uMKK28bMuY
----------------------------------------------
*/
#ifndef blob_h
#define blob_h
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
using namespace std::chrono;
class Blob {
    public:
        // member variables
        //TODO: change crossedLaneTime -> timeToPass
        unsigned int id = 1;
        long int crossedLaneTime = 0;
        int speed = 0;
        std::string imgDir;
        std::string fullImgDir;
        cv::Rect cropRect;
        //TODO: change lanesWasRunning -> crossedLanes
        std::vector< std::string > lanesWasRunning;
        high_resolution_clock::time_point crossedLineTime[2];
        std::vector<cv::Point> currentContour;
        bool cropFlag = false;
        bool linkedToMemory = false;
        bool *wroteToFile;
        bool isFullFrame = false;
        cv::Rect currentBoundingRect;
        std::vector<cv::Point> centerPositions;
        double dblCurrentDiagonalSize;
        double dblCurrentAspectRatio;
        bool blnCurrentMatchFoundOrNewBlob;
        bool blnStillBeingTracked;
        int intNumOfConsecutiveFramesWithoutAMatch;
        cv::Point predictedNextPosition;

        // function prototypes
        Blob(std::vector<cv::Point> _contour);
        Blob();
        void predictNextPosition(void);
};

#endif    // MY_BLOB


