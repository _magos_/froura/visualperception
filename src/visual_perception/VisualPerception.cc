#include "VisualPerception.h"

//#define RUN_FRAME_BY_FRAME

VisualPerception::VisualPerception(cv::VideoCapture *visual, char* sessionInfo)
{
    this->visual = visual;
    if( !setSessionName() )
        std::cerr << "Cannot set sessionName\n";
    if( !initLogFolder(sessionInfo) )
        std::cerr << "Cannot init Log folder\n";
    int codec = VideoWriter::fourcc('M', 'J', 'P', 'G');
    //if( !initVideoWriter( cv::Point(640,260), visual->get(CV_CAP_PROP_FPS), codec ) )
    //if( !initVideoWriter( cv::Point(visual->get(CV_CAP_PROP_FRAME_WIDTH),visual->get(CV_CAP_PROP_FRAME_HEIGHT) - 100), visual->get(CV_CAP_PROP_FPS), visual->get(CV_CAP_PROP_FOURCC) ) )
    //    std::cerr << "Cannot init video writer\n";

}

bool VisualPerception::initLogFolder(char* sessionInfo)
{
    //TODO: use C++ library to mkdir
    logDirectory = "log/" + sessionName + "/";
    cropDirectory = logDirectory + "crops/";
    fullImageDirectory = logDirectory + "fullImages/";
    
    //Update loginfo.csv
    ofstream f("log/loginfo.csv", std::ios::app );
    f << sessionName << "," << sessionInfo << "\n";
    //Generate necessary folder

    char* cmd = stringToCharArray( "mkdir " + logDirectory );
    system( cmd );

    cmd = stringToCharArray( "mkdir " + cropDirectory );
    system( cmd );

    cmd = stringToCharArray( "mkdir " + fullImageDirectory );
    system( cmd );
    
    delete cmd;
    return true;
}
bool VisualPerception::setSessionName()
{
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);

    std::stringstream name;
    name << std::put_time( std::localtime(&now_c), "%F_%T" );
    sessionName = name.str();
    return true;
}

//TODO: make this function static
char* VisualPerception::stringToCharArray(std::string input)
{
    char* ret = (char*) calloc( input.length(), sizeof(char) );
    std::strcpy( ret, input.c_str() );
    return ret;
}
bool VisualPerception::initVideoWriter(cv::Point size, double fps, double fourcc)
{
    char* rawDirStr = stringToCharArray( logDirectory + "raw.avi" );
    char* runningDirStr = stringToCharArray( logDirectory + "running.avi" );
    
    /*runningWriter = new cv::VideoWriter( runningDirStr, fourcc, fps, size, true );
    rawWriter = new cv::VideoWriter( rawDirStr, fourcc, fps, size, true );*/

    runningWriter = new cv::VideoWriter( runningDirStr, fourcc, fps, cv::Point(640,260), true );
    rawWriter = new cv::VideoWriter( rawDirStr, fourcc, fps, cv::Point(640,480), true );
    //delete rawDirStr;
    //delete runningWriter;

    return true;
}

VisualPerception::~VisualPerception(void)
{
    delete vehicleCouting;
}

void VisualPerception::process(void)
{
    std::thread thread_visual_perception([=]{processVisual();});
    std::thread thread_memory([=]{processMemory();});

    thread_visual_perception.detach();
    thread_memory.detach();
}

bool VisualPerception::checkProcess(void)
{
    return isProcessVisualDone && isProcessMemoryDone;
}

void VisualPerception::processMemory()
{
    while(1)
    {
        if (blobQueue.empty())
        {
            if (isProcessVisualDone)
                break;

            usleep(2000);
            continue;
        }

        Blob blob = blobQueue.front();
        blobQueue.pop();

        // Send information to higher visual perception.
        std::thread thread_send_to_higher_perception([=]{processSendToHigherPerception(blob);});
        thread_send_to_higher_perception.detach();
    }

    while (numberProcessSendToHigherPerception)
        usleep(1000);

    // Finishing memory process.
    isProcessMemoryDone = true;
    std::cout << "processMemory done\n";
}

void VisualPerception::processVisual(void)
{
    std::vector< cv::Point > p;
    Blob b(p);
    std::vector< LaneType > lanes;
    LanesRecognition lanesRecognition;
    lanesRecognition.getLane( visual, lanes );

    vehicleCouting = new VehicleCounting(lanes);

    while(key != 'q')
    {
        high_resolution_clock::time_point total_time1 = high_resolution_clock::now();

        char* timeLog = stringToCharArray( logDirectory + "time.log" );
        ofstream f( timeLog, std::ios::app );
        delete timeLog;

        cv::Mat frame;
        if (visual->read(frame) == false)
            break;

        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        processFrame(frame, t1);
        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        std::cout << "[I]" << " APTPF\t" << duration_cast<microseconds>( t2 - t1 ).count() << "\t-\talgorithm process time per frame" << "\n";

#ifdef RUN_FRAME_BY_FRAME
        key = cv::waitKey(0);
#endif

        high_resolution_clock::time_point total_time2 = high_resolution_clock::now();
        auto total_duration = duration_cast<microseconds>( total_time2 - total_time1 ).count();
        f << total_duration << "\n";
        f.close();

        std::cout << "[I]" << " TPTPF\t" << total_duration << "\t-\ttotal process time per frame" << "\n";
    }

    // Finishing visual process.
    isProcessVisualDone = true;
    std::cout << "processVisual done\n";
}

void VisualPerception::processSendToHigherPerception(Blob blob)
{
    numberProcessSendToHigherPerception++;

    if( blob.isFullFrame )
    {
        std::vector< VehicleType > vehicles;
        sendInformationToHigherPerception( blob, vehicles );
        trafficManager.checkFreeze( vehicles );
        if( vehicles.size() )
            for( auto vehicle:vehicles )
            {
                std::cout << "DETECT FREEZE: " << blob.id << "\n";
                //memory.add(blob.id, vehicle);
                //TODO: use memory manager vehicle
                logVehicleInfo( blob.id, vehicle, memory.getPercentVehicleOnRoad() );
            }
    }
    else
    {
        std::vector< VehicleType > vehicles;
        sendInformationToHigherPerception( blob, vehicles );

        vehicles[0].speed = blob.speed;
        vehicles[0].track = blob.centerPositions;
        vehicles[0].crossedLanes = blob.lanesWasRunning;

        trafficManager.checkTrafficProblem( vehicles[0] );

        //memory.add( blob.id, vehicles[0] );

        logVehicleInfo( blob.id, vehicles[0], memory.getPercentVehicleOnRoad() );
    }

    numberProcessSendToHigherPerception--;
}

void VisualPerception::processFrame(cv::Mat imgInput, high_resolution_clock::time_point gotFrameTime)
{
    std::vector< cv::Rect > boundings;
    std::string fullImgDir;
    static cv::Mat processedFrame;
    if( !vehicleCouting->process(imgInput, gotFrameTime, processedFrame) )
    {
        std::cerr << "Vehicle Counting process error";
    }
    /*if( !processedFrame.empty() )
    {
        runningWriter->write( processedFrame );
    }*/
    //TODO: use get method instead of this dirty way
    int percent = 0;
    if( vehicleCouting->getPercentVehicleOnRoad(percent) )
        memory.updatePercentVehicleOnRoad(percent);
    
    if( vehicleCouting->blobs.size() )
    {
        for( auto& blob:vehicleCouting->blobs )
        {
            if( blob.cropFlag )
            {
                blob.cropFlag = false;
                // Save original image to memory (SD Card).
                std::string imageName = "image_" + std::to_string(blob.id) +  ".jpg";
                cv::imwrite( fullImageDirectory + imageName, imgInput );
                //TODO: use fullImgDir for entire blob
                fullImgDir = fullImageDirectory + imageName;
                blob.cropRect = blob.currentBoundingRect;
                blob.cropRect.y += 100;
            }
            if( (blob.speed != 0 /*|| !blob.blnStillBeingTracked*/) && !blob.linkedToMemory )
            {
                blob.linkedToMemory = true;
                //std::cout << "Push blob ID: " << blob.id << " speed: " << blob.speed << "\n";
                blobQueue.push(blob);
            }
        }
    }
    static high_resolution_clock::time_point prevFreezeTimePoint = high_resolution_clock::now();
    high_resolution_clock::time_point currFreezeTimePoint = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>( currFreezeTimePoint - prevFreezeTimePoint ).count();
    if( duration >= 5000 )
    {
        Blob tempBlob;
        int id = 0;
        if( vehicleCouting->blobs.size() )
            id = vehicleCouting->blobs.back().id + 1;
        tempBlob.id = id;
        tempBlob.isFullFrame = true;
        tempBlob.imgDir = fullImageDirectory + "image_" + std::to_string(id) + ".jpg";
        cv::imwrite( tempBlob.imgDir, imgInput );
        tempBlob.fullImgDir = tempBlob.imgDir;
        tempBlob.cropRect = cv::Rect( 0,0, imgInput.cols, imgInput.rows );

        blobQueue.push(tempBlob);

        prevFreezeTimePoint = currFreezeTimePoint;
    }
    
}
void VisualPerception::sendInformationToHigherPerception(Blob blob, std::vector< VehicleType >& vehicles)
{
    std::string readBuffer;
    std::string imageName = "image_" + std::to_string(blob.id) +  ".jpg";
    std::string fullImgDir = fullImageDirectory + imageName;
    std::string cropDir = cropDirectory + imageName;

    cv::Mat cropped = crop( fullImgDir, blob.cropRect );
    cv::imwrite( cropDir, cropped );

    std::string imgBinary = readImgBinary( cropDir );
    std::cout << "Send " << cropDir << " to server" << "\n";

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    postImgToServer( 1, imgBinary, readBuffer );
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    std::cout << "[I]" << " STHVP\t" << duration_cast<microseconds>(t2 - t1).count() << "\t-\tsend to higher perception time" << "\n";

    //readBuffer = "none:0:0:0:0:0:[]";
    vehicleClassificationResponseParser( readBuffer, vehicles );
    for( auto& vehicle:vehicles )
    {
        vehicle.imgDir = cropDir;
        vehicle.fullImgDir = fullImgDir;
    }
}

bool VisualPerception::vehicleClassificationResponseParser(std::string res, std::vector< VehicleType >& vehicles)
{
    std::vector< std::string > vehicleInfos;

    if (res.length() > 1000)
        res = "none:0:0:0:0:0:[]";

    if( res == ":::::::[" )
        res = "none:0:0:0:0:0:[]";

    split( res, "#", vehicleInfos );

    for( auto vehicleInfo:vehicleInfos )
    {
        if( vehicleInfo == ":::::::[" )
            vehicleInfo = "none:0:0:0:0:0:[]";

        VehicleType vehicle;
        vehicleClassificationResponseParser( vehicleInfo, vehicle );
        vehicles.push_back( vehicle );
    }
}

bool VisualPerception::vehicleClassificationResponseParser(std::string res, VehicleType& vehicle)
{
    std::vector< std::string > info;
    split( res, ":", info );
    vehicle.type = info[0];
    vehicle.typeConf = info[1];
    vehicle.coord[0] = stoi( info[2] );
    vehicle.coord[1] = stoi( info[3] );
    vehicle.coord[2] = stoi( info[4] );
    vehicle.coord[3] = stoi( info[5] );
    vehicle.plate = info[6];

    return true;
}
bool VisualPerception::split( std::string input, std::string delimiter, std::vector< std::string >& output)
{
    if( input.length() == 0 )
    {
        std::cerr << "Input of split function is empty\n";
        return false;
    }

    if( delimiter.length() == 0 )
    {
        std::cerr << "Seperate string of split function is empty\n";
        return false;
    }

    if( input.find(delimiter,0) == std::string::npos )
    {
        output.push_back( input );
        return true;
    }
    else
    {
        std::string token;
        size_t pos = 0;
        while( (pos = input.find(delimiter)) != std::string::npos ) 
        {
            token = input.substr(0, pos);
            output.push_back( token );
            input.erase(0, pos + delimiter.length());
        }
        if( input.length() != 0 )
            output.push_back( input );
        return true;
    }
}

cv::Mat VisualPerception::crop( std::string img_dir, cv::Rect bounding )
{
    cv::Mat img = cv::imread(img_dir, CV_LOAD_IMAGE_COLOR);
    if( !img.empty() )
    {
        cv::Mat cropped = img(bounding);
        return cropped;
    }
    else
        std::cerr << img_dir << ": no such file or empty image" << "\n";
}

std::string VisualPerception::readImgBinary(std::string img_dir)
{
    // Encode img with base64
    std::string base64_cmd = "openssl base64 -in " + img_dir + " -out decoded.txt";
    char cmd[base64_cmd.length()];
    std::strcpy(cmd, base64_cmd.c_str());
    system(cmd);
    // Read encoded file

    std::ifstream File("decoded.txt", std::ios::in | std::ios::binary | std::ios::ate);
    if (File)
    {
        File.seekg(0);
        std::string binary( (std::istreambuf_iterator<char>(File) ),(std::istreambuf_iterator<char>() ) );
        File.close();
        system("rm decoded.txt");
        return binary;
    }
}
static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

bool VisualPerception::postImgToServer(int number_of_vehicle, std::string img_binary, std::string &readBuffer)
{
    CURL *curl;
    CURLcode res;
    curl_global_init(CURL_GLOBAL_ALL);

    //get a curl handle
    curl = curl_easy_init();
    if(!curl)
        return false;

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "charsets: utf-8");

    //curl_easy_setopt(curl, CURLOPT_URL, "192.168.0.103:3389/");
    //curl_easy_setopt(curl, CURLOPT_URL, "35.187.237.169:3389/");
    curl_easy_setopt(curl, CURLOPT_URL, "localhost:3389/");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
    // we pass our 'chunk' struct to the callback function
    // Now specify the POST data
    std::string json = "{\"number_of_vehicle\":" + std::to_string(number_of_vehicle) + ",\"vehicles\":\"" + img_binary + "\"}";
    char data[json.length()];
    std::strcpy(data, json.c_str());
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    //Perform the request, res will get the return code
    res = curl_easy_perform(curl);
    readBuffer.erase(readBuffer.begin());
    readBuffer.pop_back();
    readBuffer.pop_back();
    if( res != CURLE_OK )
        readBuffer = "none:0:0:0:0:0:none";
    std::cout << "ReadBuffer: " << readBuffer << "\n";
    // always cleanup
    curl_easy_cleanup(curl);

    curl_global_cleanup();
    return 0;
}
bool VisualPerception::logVehicleInfo(unsigned int _id, VehicleType &vehicle, int _trafficFlow )
{
    std::string id = std::to_string(_id);
    std::string type = vehicle.type;
    std::string typeConf = vehicle.typeConf;
    std::string plate = vehicle.plate;
    //std::cout << "ID: " << id << "\n";
    //std::cout << "type: " << type << "\n";
    //std::cout << "Plate: " << plate << "\n";
    std::string imgDir = vehicle.imgDir;
    std::string fullImgDir = vehicle.fullImgDir;
    std::string trafficFlow = std::to_string(_trafficFlow);
    std::string speed = "",
                track = "",
                crossedLanes = "",
                problems = "";
    if( vehicle.speed )
    {
        speed = std::to_string(vehicle.speed);
    }
    if( vehicle.track.size() != 0 )
    {
        if( vehicle.track.front().y > vehicle.track.back().y )
            track = "up";
        else
            track = "down";
    }
    if( vehicle.crossedLanes.size() != 0 )
    {
        for( auto lane:vehicle.crossedLanes )
        {
            crossedLanes += lane + "-";
        }
        crossedLanes.pop_back();
    }
    for( auto problem:vehicle.problems )
    {
        problems += problem + "-";
    }
    if( problems.length() )
        problems.pop_back();
    else 
        problems = "None";
    std::string dataFile = id + "," + type
                          + "," + typeConf
                          + "," + plate
                          + "," + imgDir
                          + "," + fullImgDir
                          + "," + speed
                          + "," + track
                          + "," + crossedLanes
                          + "," + problems
                          + "\r\n";
    std::string dataServer = "   ID: " + id
                           + "   TYPE: " + type
                           + "   SPEED: " + speed 
                           + "   BUGS: " + problems
                           + "   TRAFFIC_FLOW: " + trafficFlow
                           + "\r\n";

    writeVehicleLogToFile(dataFile);
    sendVehicleLogToServer(dataServer);
    //vehicle.wroteToFile = true;
    //std::cout << "Writing id: " << id << "..Done" << "\n";
}
bool VisualPerception::writeVehicleLogToFile( std::string log )
{
    char* vehicleLog = stringToCharArray( logDirectory + "vehicleLog.csv" );
    ofstream f( vehicleLog, std::ios::app );
    delete vehicleLog;
    f << log;
    f.close();

}
bool VisualPerception::sendVehicleLogToServer( std::string log )
{
    CURL *curl;
    CURLcode res;
    curl_global_init(CURL_GLOBAL_ALL);

    //get a curl handle
    curl = curl_easy_init();
    if(curl)
    {
        struct curl_slist *headers = NULL;
        headers = curl_slist_append(headers, "Accept: application/json");
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "charsets: utf-8");

        //curl_easy_setopt(curl, CURLOPT_URL, "192.168.0.103:3389/");
        curl_easy_setopt(curl, CURLOPT_URL, "localhost:3398");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        char data[log.length()];
        std::strcpy(data, log.c_str());
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
        //Perform the request, res will get the return code
        res = curl_easy_perform(curl);
        // always cleanup
        curl_easy_cleanup(curl);
    }
    curl_global_cleanup();
    return 0;

}
