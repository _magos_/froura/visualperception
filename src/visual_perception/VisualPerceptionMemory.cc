#include "VisualPerceptionMemory.h"

VisualPerceptionMemory::VisualPerceptionMemory(void)
{

}

VisualPerceptionMemory::~VisualPerceptionMemory(void)
{

}

bool VisualPerceptionMemory::updateVehicleTracking(unsigned int id, std::string imgDir, std::string fullImgDir, int speed, std::vector< cv::Point > track, std::vector< std::string > crossedLanes, bool* wroteToFile)
{
    std::map<unsigned int, VehicleType>::iterator it = vehiclesMap.find( id );
    if( it != vehiclesMap.end() )
    {
        std::cout << "\nMemory update vehicleInfo: " << id << " speed: " << speed;
        //std::cout << "\nType: " << it->second.type;
        it->second.imgDir = imgDir;
        it->second.fullImgDir = fullImgDir;
        it->second.speed = speed;
        it->second.track = track;
        it->second.crossedLanes = crossedLanes;
        wroteToFile = &it->second.wroteToFile;
    }
    else
    {
        std::cout << "\nMemory insert vehicleInfo: " << id << " speed: " << speed;
        VehicleType vehicle;
        vehicle.imgDir = imgDir;
        vehicle.fullImgDir = fullImgDir;
        vehicle.speed = speed;
        vehicle.track = track;
        vehicle.crossedLanes = crossedLanes;
        wroteToFile = &vehicle.wroteToFile;
        add(id, vehicle);
    }
}
bool VisualPerceptionMemory::add(unsigned int id, VehicleType vehicle)
{
    std::cout << "\nAdd blob: " << id;
    vehiclesMap[id] = vehicle;
}
bool VisualPerceptionMemory::push_back( VehicleType vehicle )
{
    //unsigned int id = vehiclesMap.rbegin()->first;
    std::pair< unsigned int, VehicleType > ele = *vehiclesMap.rbegin();
    //add( id, vehicle );
}
bool VisualPerceptionMemory::updatePercentVehicleOnRoad(int percent)
{
    percentVehicleOnRoad = percent;
}

int VisualPerceptionMemory::getPercentVehicleOnRoad()
{
    return percentVehicleOnRoad;
}

bool VisualPerceptionMemory::updateClassificationInfo( unsigned int id, std::string type, std::string confident, std::string plate )
{
    std::map<unsigned int, VehicleType>::iterator it = vehiclesMap.find( id );
    if( it != vehiclesMap.end() )
    {
        std::cout << "\nMemory update Class info: " << id;
        std::cout << "\nC " << it->second.speed;
        it->second.type = type;
        it->second.typeConf = confident;
        it->second.plate = plate;
    }
    else
    {
        std::cout << "\nMemory insert Class info: " << id;
        VehicleType vehicle;
        vehicle.type = type;
        vehicle.typeConf = confident;
        vehicle.plate = plate;
        vehiclesMap[id] = vehicle;
    }
}

VehicleType VisualPerceptionMemory::getVehicle(unsigned int id)
{
    return vehiclesMap[id];
}
bool VisualPerceptionMemory::setProblems( unsigned int id, std::vector< std::string > problems )
{
    vehiclesMap[id].problems = problems;
    return true;
}
bool VisualPerceptionMemory::getPercentVehicleOnRoad(int &percent)
{
    if( this->percentVehicleOnRoad );
    {
        percent = this->percentVehicleOnRoad;
        return true;
    }
    return false;
}
bool VisualPerceptionMemory::pop(unsigned int& id, VehicleType &vehicle)
{
    for( auto v:vehiclesMap )
    {
        if( v.second.speed != 0 )
        //if( v.second.speed != 0 && v.second.type.length() != 0 )
        {
            id = v.first;
            vehicle = v.second;
            vehiclesMap.erase(id);
            std::cout << "\nID: " << id 
                      << "\nspeed: " << v.second.speed;
            return true;
        }
    }
    return false;
}
