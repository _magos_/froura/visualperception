#pragma once

#include <unistd.h>
#include <chrono>

#include <opencv2/opencv.hpp>

#define VISUALEMULATOR_MODE_NORMAL      0
#define VISUALEMULATOR_MODE_30FPS       1
#define VISUALEMULATOR_MODE_20FPS       2
#define VISUALEMULATOR_MODE_16FPS       3
#define VISUALEMULATOR_MODE_10FPS       4

#define VISUALEMULATOR_MODE_30FPS__1_FRAME_TIME   33333
#define VISUALEMULATOR_MODE_20FPS__1_FRAME_TIME   50000
#define VISUALEMULATOR_MODE_16FPS__1_FRAME_TIME   62500
#define VISUALEMULATOR_MODE_10FPS__1_FRAME_TIME   100000

class VisualEmulator: public cv::VideoCapture
{
public:
    void set_emulate(unsigned int mode);
    unsigned int get_emulate(void);
    bool read(cv::OutputArray image);

protected:

private:
    unsigned int emulate_mode = VISUALEMULATOR_MODE_NORMAL;
    bool emulate_first_time = true;
    unsigned int frame_counter = 0;
    std::chrono::high_resolution_clock::time_point time_now;
    std::chrono::high_resolution_clock::time_point time_before;

    void emulate(void);
    void skip_image(int image_number);

};

