#pragma once

#include <iostream>
#include <thread>
#include <chrono>
#include <unistd.h>

#include <opencv2/opencv.hpp>
#include <curl/curl.h>

#include "vehicle_counting/VehicleCounting.h"
#include "VisualPerceptionMemory.h"
#include "lanes_recognition/LanesRecognition.h"
#include "traffic_manager/TrafficManager.h"

using namespace cv;
using namespace std::chrono;

typedef std::queue< Blob > BlobQueueType;
typedef std::queue< FreezeVehicleType > FreezeQueueType;

class VisualPerception
{
public:
    bool checkProcess(void);
    VisualPerception(cv::VideoCapture *visual, char* sessionInfo);
    ~VisualPerception(void);
    void process(void);

private:
    bool isProcessVisualDone = false;
    bool isProcessMemoryDone = false;
    int numberProcessSendToHigherPerception = 0;

    std::string sessionName;
    int key = 0;
    cv::VideoCapture *visual;
    VisualPerceptionMemory memory;
    VehicleCounting *vehicleCouting;
    TrafficManager trafficManager;
    char* stringToCharArray(std::string input);
    std::string logDirectory;
    std::string cropDirectory;
    std::string fullImageDirectory;
    cv::VideoWriter *rawWriter;
    cv::VideoWriter *runningWriter;
    
    void processFrame(cv::Mat frame, high_resolution_clock::time_point gotFrameTime);
    void processVisual(void);
    void processMemory();
    void processSendToHigherPerception(Blob blob);

    BlobQueueType blobQueue;
    FreezeQueueType freezeQueue;

    bool setSessionName();
    bool initVideoWriter(cv::Point size, double fps, double fourcc);
    bool initLogFolder(char* sessionInfo);
    bool initCompressionParams();

    bool logVehicleInfo( unsigned int id, VehicleType &vehicle );
    bool logVehicleInfo(unsigned int _id, VehicleType &vehicle, int trafficFlow );
    bool writeVehicleLogToFile(std::string log);
    bool sendVehicleLogToServer(std::string log);
    void sendInformationToHigherPerception( void );
    void sendInformationToHigherPerception(Blob blob, std::vector< VehicleType >& vehicle);
    cv::Mat crop( std::string img_dir, cv::Rect bounding );
    std::string readImgBinary( std::string img_dir );
    bool postImgToServer(int number_of_vehicle, std::string img_binary, std::string &readBuffer);
    
    bool vehicleClassificationResponseParser(std::string res, std::vector< VehicleType >& vehicles);
    bool vehicleClassificationResponseParser(std::string res, VehicleType& vehicle);
    bool split( std::string input, std::string delimiter, std::vector< std::string >& output);
};

