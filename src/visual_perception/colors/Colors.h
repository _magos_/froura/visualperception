#ifndef colors_h
#define colors_h
#include <opencv2/opencv.hpp>

namespace CL
{
    const cv::Scalar SCALAR_BLACK = cv::Scalar(0.0, 0.0, 0.0);
    const cv::Scalar SCALAR_WHITE = cv::Scalar(255.0, 255.0, 255.0);
    const cv::Scalar SCALAR_YELLOW = cv::Scalar(0.0, 255.0, 255.0);
    const cv::Scalar SCALAR_GREEN = cv::Scalar(0.0, 200.0, 0.0);
    const cv::Scalar SCALAR_RED = cv::Scalar(0.0, 0.0, 255.0);
}

#endif /* colors_h */

