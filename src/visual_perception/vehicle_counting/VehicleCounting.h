#ifndef vehicle_counting_h
#define vehicle_counting_h

using namespace std;
#include <chrono>
#include "blob/Blob.h"
#include <fstream>
#include <string>
#include <iomanip>
#pragma warning(disable : 4996)
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include "lanes_recognition/LanesRecognition.h"
#include "colors/Colors.h"
#include "road_lane/RoadLane.h"
#define SHOW_STEPS // un-comment | comment this line to show steps or not
#define DISTANCE_BETWEEN_TWO_LINE 7200000
using namespace std::chrono;

//TODO: Change to Lane
//  (0,0)
//
//          0------1
//          |      |
//          |      |
//          3------2
//
//
struct CrossingLineType
{
    std::string name;
    cv::Point coord[2];
};

struct VehicleCropType
{
    cv::Mat image;
    std::vector< cv::Rect > bounding; 
};
enum Position
{
    BeforeSpeedArea = 0,
    SpeedArea, // Mid-area, where use to measure speed
    AfterSpeedArea
};
struct VehiclePos
{
    std::string laneName;
    Position pos;
};
class VehicleCounting
{
    private:
        int blobIDCounter = 0;
        cv::VideoCapture capVideo;
        cv::Rect ROI;
        //TODO: Change to Lane
        std::vector< LaneType > lanes;
        std::vector< cv::Rect > boundings;
        int percentVehicleOnRoad;
        // function prototypes
        bool showCrossingLine(cv::Mat imgFrame2Copy);
        bool getVehiclePos(cv::Point centerCoord, VehiclePos &pos);
        void matchCurrentFrameBlobsToExistingBlobs(std::vector<Blob> &currentFrameBlobs);
        void addBlobToExistingBlobs(Blob &currentFrameBlob, int &intIndex);
        void addNewBlob(Blob &currentFrameBlob);
        double distanceBetweenPoints(cv::Point point1, cv::Point point2);
        void drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours, std::string strImageName);
        void drawAndShowContours(cv::Size imageSize, std::vector<Blob> blobs, std::string strImageName);
        bool checkBlobsCrossedAnyLine(high_resolution_clock::time_point gotFrameTime);
        void drawBlobInfoOnImage(std::vector<Blob> &blobs, cv::Mat &imgFrame2Copy);
        void drawCarCountOnImage(int &carCountRight, cv::Mat &imgFrame2Copy);
        void drawSpeed(cv::Mat frame);
        void removeUnnecessaryBlob();
        // global variables
        std::stringstream date;
        int carCountLeft, intVerticalLinePosition, carCountRight = 0;
    public:
        //TODO: move blobs to private and dev get method that can reference
        std::vector< Blob > blobs;
        VehicleCounting(std::vector< LaneType > lanes);
        ~VehicleCounting();

        bool process(cv::Mat imgFrame2, high_resolution_clock::time_point gotFrameTime, cv::Mat &processedFrame);
        bool getBoundings(std::vector< cv::Rect > &bounding);
        bool clearBoundings();
        bool getPercentVehicleOnRoad(int& percentVehicleOnRoad);
        bool getBlobs(std::vector< Blob > *blobs);

};
#endif /* vehicle_counting_h */

