#include "VehicleCounting.h"
#define DEBUG
//#define TIMEDEBUG
VehicleCounting::~VehicleCounting()
{
}

VehicleCounting::VehicleCounting(std::vector< LaneType > lanes)
{
    if( lanes.size() )
    {
        ROI.x = 0;
        ROI.y = 100;
        ROI.height = 260;
        ROI.width = 640;
        int i = 0;
        this->lanes = lanes;
        for( auto &lane:this->lanes )
        {
            lane.name = "lane_" + std::to_string(i);
            lane.a.y -= ROI.y;
            lane.b.y -= ROI.y;
            lane.c.y -= ROI.y;
            lane.d.y -= ROI.y;
            i++;
        }
    }
    else
    {
        ROI.x = 0;
        ROI.y = 100;
        ROI.height = 260;
        ROI.width = 640;
        std::cerr << "\nNo such crossing line";
        LaneType lane;
        lane.name = "lane_a";
        lane.a = cv::Point(0,100);
        lane.b = cv::Point(640,100);
        lane.d = cv::Point(0,200);
        lane.c = cv::Point(640,200);
        this->lanes.push_back(lane);
    }
}


bool VehicleCounting::process(cv::Mat _currFrame, high_resolution_clock::time_point gotFrameTime, cv::Mat &processedFrame)
{
    if( _currFrame.empty() )
    {
        std::cerr << "\nInput frame is empty";
        return false;
    }
#ifdef TIMEDEBUG
    high_resolution_clock::time_point t2, t3, t4;
#endif
    cv::Mat currFrame = _currFrame(ROI);
    std::vector< unsigned int*> coord;
    static bool blnFirstFrame = true;
    static bool firstFrame = true;
    static cv::Mat prevFrame;
    if( firstFrame )
    {
        prevFrame = currFrame.clone();
        cv::cvtColor(prevFrame, prevFrame, CV_BGR2GRAY);
        cv::GaussianBlur(prevFrame, prevFrame, cv::Size(5, 5), 0);

        firstFrame = false;
    }
    else
    {
        std::vector<Blob> currentFrameBlobs;
        cv::Mat imgDifference;
        cv::Mat imgThresh;
        cv::Mat rawFrame = currFrame.clone();
        cv::cvtColor(currFrame, currFrame, CV_BGR2GRAY);
        cv::GaussianBlur(currFrame, currFrame, cv::Size(5, 5), 0);
        cv::absdiff(prevFrame, currFrame, imgDifference);

        cv::threshold(imgDifference, imgThresh, 30, 255.0, CV_THRESH_BINARY);
#ifdef TIMEDEBUG
        t2 = high_resolution_clock::now();
        std::cout << "\nBefore check time: " << duration_cast<microseconds>(t2 - t1).count();
#endif

#ifdef DEBUG
        //cv::imshow("imgThresh", imgThresh);
#endif
        cv::Mat structuringElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15, 15));
        cv::Mat structuringElementx = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7));

        cv::dilate(imgThresh, imgThresh, structuringElement);
        //cv::erode(imgThresh, imgThresh, structuringElementx);

#ifdef DEBUG
        //cv::imshow("Retouched", imgThresh);
#endif
        cv::Mat imgThreshCopy = imgThresh.clone();
        std::vector<std::vector<cv::Point> > contours;
        cv::findContours(imgThreshCopy, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

#ifdef DEBUG
        //drawAndShowContours(imgThresh.size(), contours, "imgContours");
#endif
        std::vector<std::vector<cv::Point> > convexHulls(contours.size());

        for (unsigned int i = 0; i < contours.size(); i++) {
            cv::convexHull(contours[i], convexHulls[i]);
        }

        //drawAndShowContours(imgThresh.size(), convexHulls, "imgConvexHulls");
        int totalVehiclePixel = 0;
        for (auto &convexHull : convexHulls) 
        {
            Blob possibleBlob(convexHull);

            totalVehiclePixel += possibleBlob.currentBoundingRect.area();

            if (possibleBlob.currentBoundingRect.area() > 400 &&
                    possibleBlob.dblCurrentAspectRatio > 0.2 &&
                    possibleBlob.dblCurrentAspectRatio < 4.0 &&
                    possibleBlob.currentBoundingRect.width > 30 &&
                    possibleBlob.currentBoundingRect.height > 30 &&
                    possibleBlob.dblCurrentDiagonalSize > 60.0 &&
                    (cv::contourArea(possibleBlob.currentContour) / (double)possibleBlob.currentBoundingRect.area()) > 0.50)
            {
                //                    possibleBlob.cropFlag = true;
                possibleBlob.id = blobIDCounter;
                blobIDCounter++;
                currentFrameBlobs.push_back(possibleBlob);
            }
        }
        //TODO: show percentVehicleOnRoad, auto adjust devided
        percentVehicleOnRoad = (float)100*totalVehiclePixel/115200;
        //std::cout << "\nVehicle take " << (100*totalVehiclePixel/(230400)) << "\% area in road";
        int intFontFace = CV_FONT_HERSHEY_SIMPLEX;
        double dblFontScale = (currFrame.rows * currFrame.cols) / 450000.0;
        int intFontThickness = (int)std::round(dblFontScale * 2.5);

        // Right way
        cv::Size textSize = cv::getTextSize(std::to_string(carCountRight), intFontFace, dblFontScale, intFontThickness, 0);
    cv::putText(rawFrame, "Mat do xe: " + std::to_string(percentVehicleOnRoad) + "%", cv::Point(0,50), intFontFace, dblFontScale, CL::SCALAR_RED, intFontThickness);

        //drawAndShowContours(imgThresh.size(), currentFrameBlobs, "imgCurrentFrameBlobs");

        if (blnFirstFrame == true) 
        {
            for (auto &currentFrameBlob : currentFrameBlobs) 
            {
                blobs.push_back(currentFrameBlob);
            }
        } 
        else 
        {
            matchCurrentFrameBlobsToExistingBlobs(currentFrameBlobs);
        }
#ifdef TIMEDEBUG
        t3 = high_resolution_clock::now();
        std::cout << "\nAfter check time: " << duration_cast<microseconds>(t3 - t2).count();
#endif
        //drawAndShowContours(imgThresh.size(), blobs, "imgBlobs");
        drawBlobInfoOnImage(blobs, rawFrame);

        checkBlobsCrossedAnyLine(gotFrameTime);
        showCrossingLine(rawFrame);
        drawCarCountOnImage(carCountRight, rawFrame);
        drawSpeed(rawFrame);
#ifdef DEBUG
        cv::imshow("currFrame", rawFrame);
        cv::waitKey(1);
#endif
        //removeUnnecessaryBlob();
        processedFrame = rawFrame.clone();
        // now we prepare for the next iteration
        currentFrameBlobs.clear();

        prevFrame = currFrame.clone();    // move frame 1 up to where frame 2 is
        blnFirstFrame = false;
    }
#ifdef TIMEDEBUG
    t4 = high_resolution_clock::now();
    std::cout << "\nEnd time: " << duration_cast<microseconds>(t4 - t3).count();
#endif
    return true;
}

void VehicleCounting::removeUnnecessaryBlob()
{
    if( blobs.size() )
    {
        for( int i = 0; i< blobs.size(); i++ )
        {
            if( !blobs[i].blnStillBeingTracked && blobs[i].wroteToFile )
            {
                std::cout << "\nErase blob: " << blobs[i].id;
                blobs.erase(blobs.begin() + i);
            }
        }
    }
}
bool VehicleCounting::getBoundings(std::vector< cv::Rect > &boundings)
{
    boundings = this->boundings;
    return true;
}

bool VehicleCounting::clearBoundings()
{
    boundings.clear();
    return true;
}

bool VehicleCounting::getPercentVehicleOnRoad(int& percentVehicleOnRoad)
{
    percentVehicleOnRoad = this->percentVehicleOnRoad;
    return true;
}
bool VehicleCounting::getBlobs(std::vector< Blob > *blobs)
{
    blobs = &this->blobs;
    return true;
}
bool VehicleCounting::showCrossingLine(cv::Mat frame)
{
    bool prevColorIsGold = false;
    int intFontFace = CV_FONT_HERSHEY_SIMPLEX;
    double dblFontScale = (frame.rows * frame.cols) / 450000.0;
    int intFontThickness = (int)std::round(dblFontScale * 3.5);
    if( lanes.size() )
        for( auto lane:lanes )
        {
            if( !prevColorIsGold )
            {
                cv::line(frame, lane.a, lane.b, CL::SCALAR_YELLOW, 1);
                cv::line(frame, lane.c, lane.d, CL::SCALAR_YELLOW, 1);
                prevColorIsGold = true;
            }
            else
            {
                cv::line(frame, lane.a, lane.b, CL::SCALAR_BLACK, 1);
                cv::line(frame, lane.c, lane.d, CL::SCALAR_BLACK, 1);
                prevColorIsGold = false;
            }
            cv::putText(frame, lane.name, lane.d, intFontFace, dblFontScale, CL::SCALAR_RED, intFontThickness);
        }
}

//TODO: Check PIP (point in polygon)
bool VehicleCounting::getVehiclePos(cv::Point centerCoord, VehiclePos &pos)
{
    int x = centerCoord.x;
    int y = centerCoord.y;
    if( y > lanes[0].d.y )
    {
        pos.pos = BeforeSpeedArea;
    }
    else if( y > lanes[0].a.y )
    {
        pos.pos = SpeedArea;
        for( auto lane:lanes )
        {
            if( x > lane.c.x && x < lane.d.x )
            {
                pos.laneName = lane.name;
                break;
            }
        }
    }
    else 
    {
        pos.pos = AfterSpeedArea;
    }

    for( auto lane:lanes )
    {
        if( x > lane.d.x && x < lane.c.x )
            pos.laneName = lane.name;
    }
    return true;
}

void VehicleCounting::matchCurrentFrameBlobsToExistingBlobs(std::vector<Blob> &currentFrameBlobs) {
    for (auto &existingBlob : blobs) {
        existingBlob.blnCurrentMatchFoundOrNewBlob = false;
        existingBlob.predictNextPosition();
    }

    for (auto &currentFrameBlob : currentFrameBlobs) {
        int intIndexOfLeastDistance = 0;
        double dblLeastDistance = 100000.0;

        for (unsigned int i = 0; i < blobs.size(); i++) {

            if (blobs[i].blnStillBeingTracked == true) {
                double dblDistance = distanceBetweenPoints(currentFrameBlob.centerPositions.back(), blobs[i].predictedNextPosition);

                if (dblDistance < dblLeastDistance) {
                    dblLeastDistance = dblDistance;
                    intIndexOfLeastDistance = i;
                }
            }
        }

        if (dblLeastDistance < currentFrameBlob.dblCurrentDiagonalSize * 0.5) {
            addBlobToExistingBlobs(currentFrameBlob, intIndexOfLeastDistance);
        }
        else {
            addNewBlob(currentFrameBlob);
        }

    }

    for (auto &existingBlob : blobs) {
        if (existingBlob.blnCurrentMatchFoundOrNewBlob == false) {
            existingBlob.intNumOfConsecutiveFramesWithoutAMatch++;
        }
        if (existingBlob.intNumOfConsecutiveFramesWithoutAMatch >= 5) {
            existingBlob.blnStillBeingTracked = false;
        }
    }
}


void VehicleCounting::addBlobToExistingBlobs(Blob &currentFrameBlob, int &intIndex) 
{
    blobs[intIndex].currentContour = currentFrameBlob.currentContour;
    blobs[intIndex].currentBoundingRect = currentFrameBlob.currentBoundingRect;
    blobs[intIndex].centerPositions.push_back(currentFrameBlob.centerPositions.back());
    blobs[intIndex].dblCurrentDiagonalSize = currentFrameBlob.dblCurrentDiagonalSize;
    blobs[intIndex].dblCurrentAspectRatio = currentFrameBlob.dblCurrentAspectRatio;
    blobs[intIndex].blnStillBeingTracked = true;
    blobs[intIndex].blnCurrentMatchFoundOrNewBlob = true;
}


void VehicleCounting::addNewBlob(Blob &currentFrameBlob) 
{
    currentFrameBlob.blnCurrentMatchFoundOrNewBlob = true;
    blobs.push_back(currentFrameBlob);
}


double VehicleCounting::distanceBetweenPoints(cv::Point point1, cv::Point point2) {    
    int intX = abs(point1.x - point2.x);
    int intY = abs(point1.y - point2.y);

    //return(sqrt(pow(intX, 2) + pow(intY, 2)));
    return( ((intX<<2) + (intY<<2))>>2 );
}


void VehicleCounting::drawAndShowContours(cv::Size imageSize, std::vector<std::vector<cv::Point> > contours, std::string strImageName) {    
    cv::Mat image(imageSize, CV_8UC3, CL::SCALAR_BLACK);
    cv::drawContours(image, contours, -1, CL::SCALAR_WHITE, -1);
#ifdef DEBUG
    cv::imshow(strImageName, image);
#endif
}


void VehicleCounting::drawAndShowContours(cv::Size imageSize, std::vector<Blob> blobs, std::string strImageName) {    
    cv::Mat image(imageSize, CV_8UC3, CL::SCALAR_BLACK);
    std::vector<std::vector<cv::Point> > contours;

    for (auto &blob : blobs) {
        if (blob.blnStillBeingTracked == true) {
            contours.push_back(blob.currentContour);
        }
    }

    cv::drawContours(image, contours, -1, CL::SCALAR_WHITE, -1);
#ifdef DEBUG
    cv::imshow(strImageName, image);
#endif
}


bool VehicleCounting::checkBlobsCrossedAnyLine(high_resolution_clock::time_point gotFrameTime)
{
    for (auto &blob : blobs) 
    {
        if (blob.blnStillBeingTracked == true && blob.centerPositions.size() >= 2) 
        {
            int prevFrameIndex = (int)blob.centerPositions.size() - 2;
            int currFrameIndex = (int)blob.centerPositions.size() - 1;

            VehiclePos prevPos, currPos;
            getVehiclePos(blob.centerPositions[prevFrameIndex], prevPos);
            getVehiclePos(blob.centerPositions[currFrameIndex], currPos);

            if( prevPos.pos == BeforeSpeedArea && currPos.pos == SpeedArea )
            {
                blob.lanesWasRunning.push_back( currPos.laneName );
                blob.crossedLineTime[0] = gotFrameTime;
                blob.cropFlag = true;
                carCountRight++;
            }
            if( prevPos.pos == SpeedArea && currPos.pos == AfterSpeedArea )
            {
                blob.lanesWasRunning.push_back(prevPos.laneName);
                blob.crossedLineTime[1] = gotFrameTime;
                blob.crossedLaneTime = duration_cast< microseconds >( blob.crossedLineTime[1] - blob.crossedLineTime[0] ).count();
                blob.speed = (int) DISTANCE_BETWEEN_TWO_LINE/(blob.crossedLaneTime);
            }
        }
    }

    return true;
}

void VehicleCounting::drawBlobInfoOnImage(std::vector<Blob> &blobs, cv::Mat &imgFrame2Copy) {
    for (unsigned int i = 0; i < blobs.size(); i++) {
        if (blobs[i].blnStillBeingTracked == true) {
            cv::rectangle(imgFrame2Copy, blobs[i].currentBoundingRect, CL::SCALAR_RED, 2);

            int intFontFace = CV_FONT_HERSHEY_SIMPLEX;
            double dblFontScale = (imgFrame2Copy.rows * imgFrame2Copy.cols) / 300000.0;
            int intFontThickness = (int)std::round(dblFontScale * 1.0);

            cv::putText(imgFrame2Copy, std::to_string(blobs[i].id), blobs[i].centerPositions.back(), intFontFace, dblFontScale, CL::SCALAR_GREEN, intFontThickness);
        }
    }
}


void VehicleCounting::drawCarCountOnImage(int &carCountRight, cv::Mat &imgFrame2Copy) 
{
    int intFontFace = CV_FONT_HERSHEY_SIMPLEX;
    double dblFontScale = (imgFrame2Copy.rows * imgFrame2Copy.cols) / 450000.0;
    int intFontThickness = (int)std::round(dblFontScale * 2.5);

    // Right way
    cv::Size textSize = cv::getTextSize(std::to_string(carCountRight), intFontFace, dblFontScale, intFontThickness, 0);
    cv::putText(imgFrame2Copy, "So luong xe: " + std::to_string(carCountRight), cv::Point(0,30), intFontFace, dblFontScale, CL::SCALAR_RED, intFontThickness);

    // Left way
    //cv::Size textSize1 = cv::getTextSize(std::to_string(carCountLeft), intFontFace, dblFontScale, intFontThickness, 0);
    //cv::putText(imgFrame2Copy, "Vehicle count:" + std::to_string(carCountLeft), cv::Point(10, 25), intFontFace, dblFontScale, CL::SCALAR_YELLOW, intFontThickness);
}

void VehicleCounting::drawSpeed(cv::Mat frame)
{
    int intFontFace = CV_FONT_HERSHEY_SIMPLEX;
    double dblFontScale = (frame.rows * frame.cols) / 450000.0;
    int intFontThickness = (int)std::round(dblFontScale * 3.5);
    for( auto blob:blobs )
    {
        if( blob.speed && blob.blnStillBeingTracked)
        {
            cv::Point coord(blob.currentBoundingRect.x, blob.currentBoundingRect.y);
            //cv::Size textSize = cv::getTextSize(std::to_string(carCountRight), intFontFace, dblFontScale, intFontThickness, 0);
            cv::putText(frame, std::to_string(blob.speed), coord, intFontFace, dblFontScale, CL::SCALAR_RED, intFontThickness);
        }
    }
}
