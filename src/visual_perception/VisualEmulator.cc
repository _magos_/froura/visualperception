#include "VisualEmulator.h"

using namespace std::chrono;

void VisualEmulator::set_emulate(unsigned int mode)
{
    emulate_mode = mode;
}

unsigned int VisualEmulator::get_emulate(void)
{
    return emulate_mode;
}

void VisualEmulator::emulate(void)
{
    if (emulate_mode == VISUALEMULATOR_MODE_NORMAL)
        return;

    if (emulate_first_time)
    {
        emulate_first_time = false;
        frame_counter = 0;
        time_before = high_resolution_clock::now();
        return;
    }

    long int duration = 0;
    int delay_time = 0;
    switch(emulate_mode)
    {
        case VISUALEMULATOR_MODE_10FPS:
            //Skip 2 image.
            skip_image(2);

            time_now = high_resolution_clock::now();
            duration = duration_cast<microseconds>( time_now - time_before ).count();

            delay_time = VISUALEMULATOR_MODE_10FPS__1_FRAME_TIME - duration;
            break;
        case VISUALEMULATOR_MODE_16FPS:
            //Skip 1 image.
            skip_image(1);

            time_now = high_resolution_clock::now();
            duration = duration_cast<microseconds>( time_now - time_before ).count();

            delay_time = VISUALEMULATOR_MODE_16FPS__1_FRAME_TIME - duration;
            break;
        case VISUALEMULATOR_MODE_20FPS:
            // Each 2 frame skip 1 frame.
            if (frame_counter == 2)
            {
                frame_counter = 0;
                skip_image(1);
            }
            else
                frame_counter++;

            time_now = high_resolution_clock::now();
            duration = duration_cast<microseconds>( time_now - time_before ).count();

            delay_time = VISUALEMULATOR_MODE_20FPS__1_FRAME_TIME - duration;
            break;
        case VISUALEMULATOR_MODE_30FPS:
            time_now = high_resolution_clock::now();
            duration = duration_cast<microseconds>( time_now - time_before ).count();

            delay_time = VISUALEMULATOR_MODE_30FPS__1_FRAME_TIME - duration;
            break;
    }
    if (delay_time > 0)
        usleep(delay_time);

    time_before = high_resolution_clock::now();
}

void VisualEmulator::skip_image(int image_number)
{
    cv::Mat image;
    while (image_number > 0)
    {
        this->cv::VideoCapture::read(image);
        image_number--;
    }
}

bool VisualEmulator::read(cv::OutputArray image)
{
    emulate();

    return this->cv::VideoCapture::read(image);
}

