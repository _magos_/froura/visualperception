#include "LanesRecognition.h"


LanesRecognition::LanesRecognition()
{

}
LanesRecognition::~LanesRecognition()
{

}
bool LanesRecognition::getLane(cv::VideoCapture *cap, std::vector< LaneType >& lanes)
{
    cv::Mat frame, whiteLane, yellowLane, LinesImg, HSV_Img;
    cv::Mat prevFrame, diff, firstFrame, BiLinesImg;
    cap->read(firstFrame);
    int boxx = 0;
    int boxy = (firstFrame.size().height)>>2;
    int boxw = firstFrame.size().width;
    int boxh = (firstFrame.size().height) - boxy;
    double rho = 1;
    double theta = CV_PI/175;
    int threshold = 55;
    double minLinLength = 1;
    double maxLineGap = 200; // Large maxLineGap for case vehicle overlay a line
    cv::Rect const box(boxx,boxy, boxw, boxh); //this mean the first corner is
    prevFrame = firstFrame(box);
    //prevFrame = firstFrame(box);
    cv::cvtColor(prevFrame, prevFrame, CV_BGR2GRAY);
    cv::GaussianBlur(prevFrame, prevFrame, cv::Size(5,5), 0);

    int frame_counter = 0;
    while(frame_counter <= 24)
    {
        if( !cap->read(frame) )
            std::cerr << "\nCannot read video frame";
        // Find move object
        cv::Mat framecopy = frame(box);

        cv::cvtColor(framecopy, framecopy, CV_BGR2GRAY);
        cv::GaussianBlur(framecopy, framecopy, cv::Size(5,5), 0);
        cv::absdiff(prevFrame, framecopy, diff);
        prevFrame = framecopy.clone();
        cv::Mat thres;
        cv::threshold(diff, thres, 80, 255.0, CV_THRESH_BINARY);
        cv::Mat structuringElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(7, 7));

        for (unsigned int i = 0; i < 4; i++) {
            cv::dilate(thres, thres, structuringElement);
            cv::dilate(thres, thres, structuringElement);
            cv::erode(thres, thres, structuringElement);
        }
        
        std::vector< std::vector< cv::Point > > contours;
        std::vector< cv::Rect > boundings;
        
        cv::findContours(thres, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
        std::vector<std::vector<cv::Point> > convexHulls(contours.size());
        for(int i = 0; i< contours.size(); i++)
            cv::convexHull(contours[i], convexHulls[i]);

        structuringElement = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(15, 15));
        for (unsigned int i = 0; i < 4; i++) {
            cv::erode(thres, thres, structuringElement);
            cv::erode(thres, thres, structuringElement);
        }
        cv::Mat ROI = frame(box);

        // convert our img to HSV Space
        cv::cvtColor(ROI, HSV_Img, CV_RGB2HSV);

         //white color thresholding
        cv::Scalar whiteMinScalar = cv::Scalar(70,0,236);
        cv::Scalar whiteMaxScalar = cv::Scalar(191,133,255);

        cv::inRange(HSV_Img, whiteMinScalar, whiteMaxScalar, whiteLane);

        //yellow color thresholding
        cv::Scalar yellowMinScalar = cv::Scalar(81,119,200);
        cv::Scalar yellowMaxScalar = cv::Scalar(101,255,255);

        cv::inRange(HSV_Img, yellowMinScalar, yellowMaxScalar, yellowLane);



        //combine our two images in one image
        cv::addWeighted(whiteLane, 1.0, yellowLane, 1.0, 0.0, LinesImg);
        cv::drawContours(LinesImg, contours, -1, CL::SCALAR_BLACK, -1);


        // Edge detection using canny detector
        int minCannyThreshold = 90;
        int maxCannyThreshold = 250;
        cv::Canny(LinesImg, LinesImg, minCannyThreshold, maxCannyThreshold, 5, true);
        // Morphological Operation
        cv::Mat k = cv::getStructuringElement(CV_SHAPE_RECT,cv::Size(9,9)); //MATLAB :k=Ones(9)

        cv::morphologyEx(LinesImg, LinesImg, cv::MORPH_CLOSE, k);
#ifdef DEBUG
        cv::imshow("LinesIm", LinesImg);
#endif
        // now applying hough transform TO dETECT Lines in our image
        std::vector<cv::Vec4i> lines;

        cv::HoughLinesP(LinesImg, lines, rho, theta, threshold, minLinLength, maxLineGap );

        static std::vector< cv::Vec4i > allline;
        allline.insert(std::end(allline), std::begin(lines), std::end(lines));
        //draw our lines
        // y = ax +b;
        BiLinesImg = cv::Mat(LinesImg.size(), CV_8UC1, CL::SCALAR_BLACK);
        for( size_t i = 0; i < allline.size(); i++ )
        {
            cv::Vec4i l = allline[i];  // we have 4 elements p1=x1,y1  p2= x2,y2
            //if( (float)abs((float) (l[0] - l[2])/(l[1] - l[3])) < 0.8 )
            //{
                cv::Scalar greenColor= cv::Scalar(0,250,30);  // B=0 G=250 R=30
                cv::line( BiLinesImg, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), CL::SCALAR_WHITE, 1, CV_AA);
                //cv::waitKey(0);
            //}
        }
#ifdef DEBUG
        cv::imshow("BiLinesImg", BiLinesImg);
        if(cv::waitKey(30)==27)
        {
            std::cout<<"esc";
            break;
        }
#endif
        frame_counter++;
    }
    std::vector< std::vector< cv::Point > > contours;
    std::vector< int* > linesCoord;
    
    cv::findContours(BiLinesImg, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    std::cout << "\nContours size: " << contours.size();
    std::vector< cv::Vec4i > lines_temp;
    std::vector< cv::Vec4i > reallines;
    for( auto contour:contours )
    {
        cv::Rect bounding = cv::boundingRect(contour);
        cv::Mat line = BiLinesImg(bounding);
        cv::HoughLinesP(line, lines_temp, rho, theta, threshold, minLinLength, maxLineGap );
        bool lineIsUp = (lines_temp[0][0] > lines_temp[0][2]) ? true:false;
        cv::Vec4i realline;
        std::cout << "\nlines_temp : " << lines_temp[0][0] 
                                       << "-" << lines_temp[0][1]
                                       << "-" << lines_temp[0][2]
                                       << "-" << lines_temp[0][3];
        //TODO: Find out why that fucking lines_temp save x at [1] and y at [0] position???
        if( lines_temp[0][1] > lines_temp[0][3] )
        {
            std::cout << "\nUp";
            realline[0] = (bounding.x + bounding.width);
            realline[1] = bounding.y + boxy;
            realline[2] = bounding.x;
            realline[3] = (bounding.y + bounding.height) + boxy;
        }
        else
        {
            std::cout << "\nNotUp";
            realline[0] = bounding.x;
            realline[1] = (bounding.y) + boxy;
            realline[2] = bounding.x + bounding.width;
            realline[3] = (bounding.y + bounding.height) + boxy;
        }
//        std::cout << "\nLine: " << realline[0] << "-" << realline[0] << "-" << realline[1] << "-" << realline[1];
        //boundings.push_back(bounding);
        reallines.push_back(realline);
    }
    BiLinesImg = cv::Mat(LinesImg.size(), CV_8UC1, CL::SCALAR_BLACK);
    for(auto l:reallines)
    {
        cv::Scalar greenColor= cv::Scalar(0,250,30);  // B=0 G=250 R=30
        cv::line( frame, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), greenColor, 2, CV_AA);
    }
#ifdef DEBUG
    cv::imshow("Img",frame);
#endif
    calculateLinesLinearEquation(reallines);
    calculateXCoord();
    lanes = this->lanes;
#ifdef DEBUG
    cv::destroyAllWindows();
#endif
    return true;
}

void LanesRecognition::convexHull(cv::Mat &imgSrc)
{
    std::vector< std::vector< cv::Point > > contours;
    
    cv::findContours(imgSrc, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);
    std::vector<std::vector<cv::Point> > convexHulls(contours.size());
    for(int i = 0; i< contours.size(); i++)
        cv::convexHull(contours[i], convexHulls[i]);
    cv::drawContours(imgSrc, convexHulls, -1, CL::SCALAR_WHITE, -1);
}

bool LanesRecognition::calculateLinesLinearEquation(std::vector< cv::Vec4i > lines)
{
    if( !lines.size() )
        return false;
    if( lines.size() < 2 )
    {
        //TODO: handle this case
        return false;
    }
    for( auto line:lines )
    {
        LinearEquationType equation;
        //y = ax + b;
        //A(x1, y1)   B(x2, y2)
        // a = (y1 - y2)/(x1 -x2)
        // b = y1 - ax1
        equation.a = (float) (line[1] - line[3])/(line[0] - line[2]);
        equation.b = line[1] - equation.a*line[0];
        lineLinearEquations.push_back( equation );
    }
    return true;
}

bool LanesRecognition::calculateXCoord()
{
    topLineYCoord = 180;
    botLineYCoord = 250;
    if( !botLineYCoord )
        return false;

    if( !topLineYCoord )
        return false;

    if( !lineLinearEquations.size() )
        return false;
    //sort_equations();
    int numberOfLine = lineLinearEquations.size();
    std::array< int, 2 > xs[numberOfLine];
    int i = 0;
    for( auto equation:lineLinearEquations )
    {
        // y = ax + b;
        // x = (y-b)/a;
        xs[i][0] = (topLineYCoord - equation.b)/equation.a;
        std::cout << "\nTX = " << xs[i][0];
        xs[i][1] = (botLineYCoord - equation.b)/equation.a;
        std::cout << "\nBX = " << xs[i][1];
        i++;
    }
    std::sort(xs, xs + numberOfLine);
    for( int i = 0; i < numberOfLine - 1; i++ )
    {
        LaneType lane;
        lane.a.y = topLineYCoord;
        lane.a.x = xs[i][0];

        lane.d.y = botLineYCoord;
        lane.d.x = xs[i][1];
        
        lane.b.y = topLineYCoord;
        lane.b.x = xs[i + 1][0];

        lane.c.y = botLineYCoord;
        lane.c.x = xs[i + 1][1];

        lanes.push_back(lane);
    }

    return true;
}
