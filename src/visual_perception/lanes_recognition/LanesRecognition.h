#ifndef lanes_recognition
#define lanes_recognition

#include <opencv2/opencv.hpp>
#include <iostream>
#include "colors/Colors.h"
#include "road_lane/RoadLane.h"
/*
struct CrossingLineType
{
    std::string name;
    cv::Point coord[2];
};
*/
/* (0,0)
 *
 *      a-------b
 *      |       |
 *      |       |
 *      |       |
 *      d-------c
 * */
struct LinearEquationType
{
    float a,b;
};
class LanesRecognition
{
    private:
        std::vector< LinearEquationType > lineLinearEquations;
        std::vector< LaneType > lanes;
        int topLineYCoord;
        int botLineYCoord;
        void convexHull(cv::Mat &imgSrc);
    public:
        LanesRecognition();
        ~LanesRecognition();
        bool calculateLinesLinearEquation(std::vector< cv::Vec4i > lines);
        bool calculateXCoord();
        bool getLane(cv::VideoCapture *cap, std::vector< LaneType >& lane);
};

#endif /* lanes_recognition */
