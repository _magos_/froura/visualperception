#pragma once

#include <iostream>

#include <opencv2/opencv.hpp>
struct VehicleType
{
    std::string type;
    std::string typeConf;
    int coord[4];
    std::string plate;
    std::string imgDir = "";
    std::string fullImgDir = "";
    int speed;
    std::vector<cv::Point> track;
    std::vector<std::string> crossedLanes;
    std::vector<std::string> problems;
    bool wroteToFile = false;
    int appearTimes = 0;
    bool appear = false;
};

class VisualPerceptionMemory
{
public:
    VisualPerceptionMemory(void);
    ~VisualPerceptionMemory(void);

    bool add(unsigned int id, VehicleType vehicle);
    bool push_back( VehicleType vehicle );
    bool updateVehicleTracking(unsigned int id, std::string imgDir, std::string fullUImgDir, int speed, std::vector< cv::Point > track, std::vector< std::string > crossedLanes, bool* wroteToFile);
    bool pop(unsigned int &id, VehicleType &vehicle);
    bool updatePercentVehicleOnRoad( int percent );
    int getPercentVehicleOnRoad();
    bool updateClassificationInfo( unsigned int id, std::string type, std::string confident, std::string plate );
    VehicleType getVehicle( unsigned int id );
    bool setProblems( unsigned int id, std::vector< std::string > problems );
    bool getPercentVehicleOnRoad( int &percent );
private:
    //std::vector<Vehicle> vehicles;
    std::map< unsigned int, VehicleType > vehiclesMap;
    int percentVehicleOnRoad;

};

