#include "TrafficManager.h"

TrafficManager::TrafficManager()
{
    readLaw();
}

TrafficManager::~TrafficManager()
{

}

bool TrafficManager::readLaw()
{
    //TODO: make database and read law from database
    trafficLaw.minSpeed = 0;
    trafficLaw.maxSpeed = 35;
    trafficLaw.trafficJamLimit = 90;
    trafficLaw.freezeTimeLimit = 5; // 30*10s = 5 min
    trafficLaw.upDirection = true;

    std::pair< bool, std::vector<std::string> > lane;
    lane.first = true;
    lane.second.push_back("motorbike");
    trafficLaw.laneRules["lane_0"] = lane;

    lane.first = false;
    lane.second.push_back("car");
    trafficLaw.laneRules["lane_1"] = lane;

    lane.first = true;
    trafficLaw.laneRules["lane_2"] = lane;

    lane.first = true;
    trafficLaw.laneRules["lane_3"] = lane;

    lane.first = true;
    trafficLaw.laneRules["lane_4"] = lane;
}

bool TrafficManager::checkTrafficProblem(VehicleType &vehicle) 
{
    if( wrongSpeed(vehicle.speed) )
        vehicle.problems.push_back( "Wrong speed" );

    wrongLane( vehicle );

    if( vehicle.problems.size() == 0 )
        vehicle.problems.push_back("none");
    return true;
}

bool TrafficManager::wrongSpeed(int speed)
{
    if( !speed )
    {
        std::cerr << "\nspeed is null";
        return false;
    }
    else if( speed < trafficLaw.minSpeed || speed > trafficLaw.maxSpeed )
        return true;
    return false;
}
//TODO: replace entire of params with VehicleType
bool TrafficManager::wrongLane( VehicleType& vehicle )
{
    bool directionIsUp = ( vehicle.track.front().y < vehicle.track.back().y ) ? true:false;
    if( vehicle.type == "none" )
        return false;
    for( auto lane:vehicle.crossedLanes )
    {
        std::pair< bool, std::vector<std::string> > validVehicles = trafficLaw.laneRules[lane];
        if( directionIsUp != validVehicles.first)
        {
            std::vector< std::string >::iterator it = find( vehicle.problems.begin(), vehicle.problems.end(), "Wrong dir"); 
            if( it == vehicle.problems.end() )
                vehicle.problems.push_back("Wrong dir");
        }
        bool wronglane = true;;
        for( auto vehicleType:validVehicles.second )
        {
            if( vehicle.type == vehicleType )
            {
                wronglane = false;
                break;
            }
        }
        if( wronglane )
        {

            std::vector< std::string >::iterator it = find( vehicle.problems.begin(), vehicle.problems.end(), "Wrong lane"); 
            if( it == vehicle.problems.end() )
                vehicle.problems.push_back("Wrong lane");
        }
    }
    return true;
}
int* TrafficManager::getCenterPoint( int* coord )
{
    // x1 y1 x2 y2 
    int ret[2];
    ret[0] = (coord[0] + coord[2])/2;
    ret[1] = (coord[1] + coord[3])/2;
    return ret;
}

bool TrafficManager::checkFreeze( FreezeVehicleType& vehicles )
{
    for( auto vehicle:vehicles )
    {
        if( vehicle.type == "none" )
            continue;
        int* centerPoint = getCenterPoint( vehicle.coord );
        if( freezeMap.find(centerPoint) == freezeMap.end() )
        {
            vehicle.appearTimes ++;
            vehicle.appear = true;
            freezeMap[centerPoint] = vehicle;
        }
        else
        {
            freezeMap[centerPoint].appearTimes ++;
            freezeMap[centerPoint].appear = true;
            std::cout << "\nUpdate freeze: " << vehicle.type << " - times: " << freezeMap[centerPoint].appearTimes;
        }
    }
    vehicles.clear();
    for( auto it = freezeMap.begin(); it != freezeMap.end(); ++it )
    {
        if( it->second.appearTimes >= trafficLaw.freezeTimeLimit )
        {
            std::vector< std::string >::iterator problemIt = find( it->second.problems.begin(), it->second.problems.end(), "Freeze"); 
            if( problemIt == it->second.problems.end() )
                it->second.problems.push_back("Freeze");
            it->second.speed = 0;
            vehicles.push_back( it->second );
        }
        if( it->second.appear == false )
            freezeMap.erase(it);
        it->second.appear = false;
    }
}
