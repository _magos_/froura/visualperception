#ifndef traffic_manager_h
#define traffic_manager_h

#include "VisualPerceptionMemory.h"
/*
 *  first:  object type
 *  second: coordinate
 * */
//static typedef std::unordered_map< std::string, int* > FreezeObjectMapType;

/*
 *  first:  lane name
 *  second: pair< direction_is_up, valid vehicles >
 * */
typedef std::unordered_map< std::string, std::pair< bool, std::vector<std::string> > > LaneRulesType;
typedef std::vector< VehicleType > FreezeVehicleType;

struct TrafficLaw
{
    int minSpeed, maxSpeed;
    int trafficJamLimit;
    int freezeTimeLimit;
    // if direction is up, upDirection = true;
    bool upDirection;
    LaneRulesType laneRules;
};
class TrafficManager
{
    private:
        TrafficLaw trafficLaw;

        bool trafficJam;
//        FreezeObjectMapType freezeObjects;
        std::map< int*, VehicleType > freezeMap;

        bool readLaw();

        bool wrongLane( VehicleType& vehicle );
        bool wrongSpeed(int speed);
        bool freezeObjectProcessing();
        bool split( std::string input, std::string delimiter, std::vector< std::string >& output);
        int* getCenterPoint(int* coord);
    public:
        TrafficManager();
        ~TrafficManager();
        bool checkTrafficProblem( VehicleType& vehicle );
        bool checkFreeze( FreezeVehicleType& vehicles );
};

#endif /* traffic_manager_h */
