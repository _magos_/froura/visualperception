#include "visual_perception/VisualPerception.h"
#include "visual_perception/VisualEmulator.h"

#define VISUAL_EMULATE_MODE VISUALEMULATOR_MODE_30FPS

int main(int argc, char *argv[])
{
    high_resolution_clock::time_point total_time1 = high_resolution_clock::now();

    std::cout << "Optikí Antílipsi\n";

    // Set up video capture tool.
    VideoCapture *capture;
    VisualEmulator emulator;
    capture = &emulator;

    // Set emulate mode.
    ((VisualEmulator*)capture)->set_emulate(VISUAL_EMULATE_MODE);

    if (argc == 1)
        capture->open( 0 );
    else
        capture->open( argv[1] );

    if ( ! capture->isOpened() )
    {
        std::cerr << "Cannot open video/camera!\n";
        return false;
    }

    VisualPerception visual_perception(capture, argv[2]);
    visual_perception.process();

    while(!visual_perception.checkProcess())
        usleep(500000);

    high_resolution_clock::time_point total_time2 = high_resolution_clock::now();

    std::cout << std::endl;
    std::cout << "[I]" << " TOTAL_TIME\t" << duration_cast<microseconds>( total_time2 - total_time1 ).count() << "\t-\ttotal executetion time of Optikí Antílipsi" << "\n";
    std::cout << std::endl;

    return 0;
}

