# Optikí Antílipsi (Visual Perception) - Frourá Project

Frourá project is a smart camera system designed for intelligent transportation system (ITS).
Optikí Antílipsi is the name of this program.
This program is responible for visual perception of camera devices in the system.

# Install
Need to make some folder to store cropped image
```
mkdir images
mkdir cropped
mkdir cropped/images
```

# Overall

This program will be run on camera devices.
It will get frames from image sensor of camera devices and do visual perception on that, expected perception includes:

- object/movement detection.
- object/movement tracking.

Then, the perceptive data will be send to information extraction program for further processing.

