#!/bin/sh

if [ "$1" = "" ]; then
    echo 'This script will generate config files for visual perception.'
    echo 'It takes one argument, which is the directory that config files will be generated.'
    echo 'Example: ./scripts/generate_config.sh ./config'
else
    echo 'config files will be generated to this directory:' $1
    cp -r scripts/config/ $1
fi

